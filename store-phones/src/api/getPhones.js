'use strict';

module.exports = phone => async (req, res) => {

    const orderList = (req.params.id) ? {'id': { $in: req.params.id.split('-')}} : {};
    return await phone.find(orderList,{ _id: 0})
                        .then(data => res.send(data))
                        .catch(err => res.send(err, 500));
}
