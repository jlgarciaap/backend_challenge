'use strict';


module.exports = {
    dbUser: encodeURIComponent(process.env.MONGO_USER || 'admin'),
    dbPassword: encodeURIComponent(process.env.PASSWORD ||'admin1234'),
    dbPort: process.env.MONGO_PORT || 27017,
    dbName: process.env.MONGO_DB_NAME || 'store',
    dbHostname: process.env.MONGO_HOSTNAME || 'mongo',
    serverPort: process.env.PORT || 3001
}