'use strict';

const { Schema, model }= require('mongoose');

const phoneSchema = new Schema({
    _id:  Schema.Types.ObjectId,
    id: String ,
    name: String,
    description: String,
    image: String,
    price: Number
});


const Phone = model('Phone', phoneSchema);

module.exports = Phone;