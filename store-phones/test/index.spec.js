'use strict';

const chai = require('chai'),
    chaiHttp = require('chai-http');
const expect = chai.expect;
const db = require('./db');
const server = require('../server');
const Mockgoose = require('mockgoose').Mockgoose;
const mockgoose = new Mockgoose(db.mongoose);

const url = 'mongodb://localhost:3002/store';

chai.use(chaiHttp);

before(function (done) {

    mockgoose.prepareStorage().then(function(){
        db.connect(url)
            .then(()=> {
               server.start(3001).then().catch(err => console.log(err));
               done()
            })
           .catch((err) => {console.log(err); done(err)});
    });

});

after(function(){
    mockgoose.helper.reset();
    db.close();
    mockgoose.mongodHelper.mongoBin.childProcess.kill('SIGTERM');
    server.stop();
})

describe('Testing phones endpoint', function(){
    it('Test succesfull request', function(done){

       chai.request('http://localhost:3001')
            .get('/phones/')
            .end(function(err, data){
                expect(err).to.be.null;
                expect(data).to.have.status(200);
                expect(data.body).to.be.an('array');
                done();
            })

    })

    it('Test succesfull request with ID params', function(done){

        chai.request('http://localhost:3001')
             .get('/phones/1001-1002')
             .end(function(err, data){
                 expect(err).to.be.null;
                 expect(data).to.have.status(200);
                 expect(data.body).to.be.an('array');
                 done();
             })

     })

     it('Test incorrect http method', function(done){

        chai.request('http://localhost:3001')
             .post('/phones/')
             .end(function(err, data){
                 expect(err).to.be.null;
                 expect(data).to.have.status(404);
                 done();
             })

     })


     it('Test inexistent endpoint', function(done){

        chai.request('http://localhost:3001')
             .get('/test/')
             .end(function(err, data){
                 expect(err).to.be.null;
                 expect(data).to.have.status(404);
                 done();
             });

     })
})

