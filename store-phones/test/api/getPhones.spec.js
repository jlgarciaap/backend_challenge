'use strict';

const { expect } = require('chai');

const sinon = require('sinon');

const getPhones = require('../../src/api/getPhones');

let req;
let res;
let models;

describe('Testing things', function(){

    before( function() {
    // runs before all tests in this block

        req = {
            params: {

            }
        };

        res = {
            send: sinon.stub().resolves([])
        };

        models = {
            Phone: {
                find: sinon.stub().resolves([])
            }
        }


    });

    it('Check return phones successfull', async ()=> {

        await getPhones(models.Phone)(req,res)
                    .then(result => {
                            expect(models.Phone.find.calledOnce).to.be.true;
                            expect(result).to.be.an('array');
                            expect(result.length).to.be.equals(0);
                        })
                    .catch(err => console.log(err));
    });

});


