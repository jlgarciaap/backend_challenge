'use strict';

const db = require('./db');
const server = require('./server');
const config = require('./src/config');

//db.connect(url)
   // .then(())
const dbUrl = `mongodb://${config.dbUser}:${config.dbPassword}@${config.dbHostname}:${config.dbPort}/${config.dbName}`;

db.connect(dbUrl).then(()=> {
    server.start(config.serverPort)
            .then(console.log('Service store-phones start on port ',config.serverPort))
            .catch((err) => {
                console.error('ERROR: Service store-phones error ', err);
                process.exit(1);
            })
        })
        .catch((err) => console.log('[ERROR]: ', err));
