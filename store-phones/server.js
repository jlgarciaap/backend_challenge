'use strict';

const service = require('restana')({ignoreTrailingSlash: true}),
    getPhones = require('./src/api/getPhones'),
    models = require('./src/models');


/**  GET PHONES **/

service.get('/phones/:id',getPhones(models.Phone));


module.exports = {
    start: (port) => service.start(port),
    stop: ()  => service.close()
}

