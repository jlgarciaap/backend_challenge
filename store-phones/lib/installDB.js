'use strict';

const dbCLient = require('mongodb').MongoClient;

const PhoneModel = {
    list: [
        {
            'id': '1001',
            'name': 'Iphone X',
            'description': 'SuperPhone by Apple',
            'image': './assets/image/iphone_x.jpg',
            'price': 1000
        },
        {
            'id': '1002',
            'name': 'Huawei Mate 10',
            'description': 'SuperPhone by Huawei',
            'image': './assets/image/mate.jpg',
            'price': 700
        },
        {
            'id': '1003',
            'name': 'Xiaomi MiMix 2',
            'description': 'SuperPhone by Xiaomi',
            'image': './assets/image/xiaomi.jpg',
            'price': 400
        },
    ]
}

function initDB() {

    const user = encodeURIComponent('admin');
    const password = encodeURIComponent('admin1234');

    const client = new dbCLient(`mongodb://${user}:${password}@mongo:27017`);

    client.connect(function(err, client) {

        if (err) {return console.dir(err)};

        const db = client.db('store');


        db.collection('phones').deleteMany({}, function(err, data) { if (err) console.log('ERROR:', err); console.log('Collection deleted', data.result) });

        db.collection('phones').insertMany(PhoneModel.list, function(err, data) { if (err) console.log('ERROR:', err); console.log('result:', data.result) });

        db.collection('phones').createIndex({ id: 1}, function(err, data) { if (err) console.log('ERROR:', err); console.log('INDEX CREATED ') } )

        db.removeUser(user);

        db.addUser(user,password,
            {
                roles: [
                { role: "readWrite", db: "store"}
            ]
        }, function(err, data) { if (err) console.log('ERROR:', err); console.log('User Created', data.result) });


        client.close();
    });
}

initDB();
