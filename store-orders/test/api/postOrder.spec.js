'use strict';

const { expect } = require('chai');
const nock = require('nock');
const response = require('./data/response');
const postOrder = require('../../src/api/postOrder');

describe('Test postOder', function (){


    const order = {
        name: 'Juan Luis',
        surname: 'Garcia',
        email: 'juan@email.com',
        list: {
            '1001': 2,
            '1002': 3
        }
    }

    const result = { name: 'Juan Luis',
                    surname: 'Garcia',
                    email: 'juan@email.com',
                    total: 4100,
                    phones:
                    [ { id: '1001',
                        name: 'Iphone X',
                        description: 'SuperPhone by Apple',
                        image: './assets/image/iphone_x.jpg',
                        price: 1000,
                        quantity: 2 },
                    { id: '1002',
                        name: 'Huawei Mate 10',
                        description: 'SuperPhone by Huawei',
                        image: './assets/image/mate.jpg',
                        price: 700,
                        quantity: 3 } ] }

    const res = {
        send: (data) => {
            return data
        }
    }

    const req = {
        body: order
    }

    before(function(){
        nock('http://store-phones:3001/')
            .get('/phones/1001-1002')
            .reply(200, response);
    })


    it('Test basic post successfull', async() => {

        await postOrder(req, res)
                .then(response => {
                    expect(response).to.be.an('Object');
                    expect(response).to.eql(result);
                })
                .catch(err => err);

    });

    it('Test basic post with one empty body param', async() => {

        req.body.email = '';

        const result = {
            error: "We need all params, recieved:",
            name: 'Juan Luis',
            surname: '',
            email: 'juan@email.com',
            list: {
                '1001': 2,
                '1002': 3
            }
        }

        await postOrder(req, res)
                .then(response => {
                    expect(response).to.be.an('Object');
                    expect(response).to.eql(result);
                })
                .catch(err => err);

    });


});
