'use strict';

const service = require('restana')({ignoreTrailingSlash: true});
const parser = require('body-parser');
const postOrder = require('./src/api/postOrder');


service.use(parser.json());

/** POST ORDER **/
service.post('/order/', postOrder);


module.exports = { 
    start: (port) => service.start(port)
}

