'use strict';

module.exports = {
    phonesHostname: process.env.PHONES_SERVICE_HOSNAME || 'store-phones',
    phonesPort: process.env.PHONES_SERVICE_PORT || 3001
}