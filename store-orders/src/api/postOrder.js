"use strict";

const axios = require("axios");
const config = require('../config');

module.exports = async (req, res) => {
  const { name, surname, email, list } = req.body;

  if (!name || !surname || !email || !list || Object.keys(list).length < 1) {
    return res.send(
      { error: "We need all params, recieved:", name, surname, email, list },
      400
    );
  }

  const order = {
    name,
    surname,
    email,
    total: 0
  };

  const uri = `http://${config.phonesHostname}:${config.phonesPort}/phones/` + Object.keys(list).join("-");
  return await axios
    .get(uri)
    .then(result => {
      order.phones = result.data.map(phone => {
        order.total = order.total + phone.price * list[phone.id];
        phone.quantity = list[phone.id];
        return phone;
      });

      return res.send(order);
    })
    .catch(err => {
      return res.send("ERROOOOOR");
    });
};
