'use strict';

const server = require('./server');

const port = process.env.PORT || 3002;

server.start(port)
    .then(console.log('Service store-orders start on port ',port))
    .catch((err) => {
        console.error('ERROR: Service store-orders error ', err);
        process.exit(1);
    });
