'use strict';

const service = require('restana')({ignoreTrailingSlash: true});
const parser = require('body-parser');
const endpoints = require('./api');


service.use(parser.json());

// ENDPOINT DEFINITIONS
const server = endpoints.register(service);


module.exports = {
    start: (port) => server.start(port),
    stop: () => server.close()
}



