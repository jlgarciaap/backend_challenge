'use strict';

const axios = require('axios');
const config = require('./config');

module.exports = async(req, res) => {

    return await axios
            .post(`http://${config.orderServiceHostname}:${config.orderServicePort}/order/`,req.body)
            .then((result) => {
                console.log(result.data);
                return res.send(result.data);
            })
            .catch((err) => {
                res.send(err.response.data, err.response.status);
            });
}