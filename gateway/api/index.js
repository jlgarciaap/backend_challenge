'use strict';

const getPhones = require('./getPhones');
const postOrder = require('./postOrder');

module.exports = {
    register: (service)  =>{
        service.get('/phones/:id', getPhones);
        service.get('/phones', getPhones);
        service.post('/order', postOrder);
        return service;
    }
}