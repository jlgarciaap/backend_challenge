'use strict';
const axios = require('axios');
const config = require('./config');

module.exports = async(req,res) => {

    const list = req.params.id ? req.params.id : '';

    return await axios
            .get(`http://${config.phoneServiceHostname}:${config.phoneServicePort}/phones/` + list)
            .then((result) => res.send(result.data))
            .catch((err) => console.log(err));
}

