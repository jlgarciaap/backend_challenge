'use strict';

module.exports = {
    phoneServicePort: process.env.PHONES_SERVICE_PORT || 3001,
    phoneServiceHostname: process.env.PHONES_SERVICE_HOSTNAME || 'store-phones',
    orderServicePort: process.env.ORDERS_SERVICE_PORT || 3002,
    orderServiceHostname: process.env.ORDERS_SERVICE_HOSTNAME || 'store-orders',
}