module.exports = {
    phones :
        [{
            id: '1001',
            name: 'Iphone X',
            description: 'SuperPhone by Apple',
            image: './assets/image/iphone_x.jpg',
            price: 1000
        },
        {
            id: '1002',
            name: 'Huawei Mate 10',
            description: 'SuperPhone by Huawei',
            image: './assets/image/mate.jpg',
            price: 700
        }]

}