'use strict';

module.exports = {

    order : { name: 'Juan Luis',
                    surname: 'Garcia',
                    email: 'juan@email.com',
                    total: 4100,
                    phones:
                    [ { id: '1001',
                        name: 'Iphone X',
                        description: 'SuperPhone by Apple',
                        image: './assets/image/iphone_x.jpg',
                        price: 1000,
                        quantity: 2 },
                    { id: '1002',
                        name: 'Huawei Mate 10',
                        description: 'SuperPhone by Huawei',
                        image: './assets/image/mate.jpg',
                        price: 700,
                        quantity: 3 } ] },
    phones: [
        {
            'id': '1001',
            'name': 'Iphone X',
            'description': 'SuperPhone by Apple',
            'image': './assets/image/iphone_x.jpg',
            'price': 1000
        },
        {
            'id': '1002',
            'name': 'Huawei Mate 10',
            'description': 'SuperPhone by Huawei',
            'image': './assets/image/mate.jpg',
            'price': 700
        },
    ]
}