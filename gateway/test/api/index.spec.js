'use strict';

const chai = require('chai'),
    chaiHttp = require('chai-http');
const expect = chai.expect;
const nock = require('nock');
const responses = require('./data/responses');
const results = require('../data/results');
const server = require('../../server');

chai.use(chaiHttp);
describe('Test gateway methods', () => {

    const order = {
        name: 'Juan Luis',
        surname: 'Garcia',
        email: 'juan@email.com',
        list: {
            '1001': 2,
            '1002': 3
        }
    }


    const res = {
        send: (data) => {
            return data
        }
    }

    const req = {
        body: order
    }

    before(function(){

        server.start(3000).then(console.log('Server Init in port 3000'))

        nock('http://store-phones:3001/')
            .get('/phones/1001-1002')
            .reply(200, responses.phones);

        nock('http://store-orders:3002')
            .post('/order/')
            .reply(200, responses.order);
    })

    it('Test get phones endpoint', () => {

        chai.request('http://localhost:3000')
            .get('/phones/1001-1002')
            .end(function(err, data){
                expect(err).to.be.null;
                expect(data).to.have.status(200);
                expect(data.body).to.be.an('array');
                expect(data.body).to.be.eql(results.phones);

            })
    });

    it('Test post order endpoint', () => {

        chai.request('http://localhost:3000')
            .post('/order/')
            .set('content-type', 'application/json')
            .send(order)
            .end(function(err, data){
                expect(err).to.be.null;
                expect(data).to.have.status(200);
                expect(data.body).to.be.an('Object');
                expect(data.body).to.be.eql(results.order);

            })
    });

});